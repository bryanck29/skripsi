# About this Project
Project is based on Rengorum, a single-page forum application built in ReactJS and Django Rest Framework.

## Installation

Make sure you have following software installed in your system:
* Python 3
* Node.js
* NPM / Yarn
* Git

Install all required dependencies in an isolated environment

```
cd skripsi/backend

/* Use python instead of python 3 if you dont have multiple instance of Python installed or your Python 3.x is set to default */
python3 -m venv venv 

//For LINUX/MAC: 
source venv/bin/activate
//For WINDOWS:
venv/Scripts/activate

pip install -r requirements.txt
python -m spacy download en
```

Copy the `.env.example` as `.env` in `backend` folder
```
On terminal/powershell
cp .env.example .env
```

Install all required dependencies for frontend in skripsi/frontend folder by typing
```
cd ../frontend
yarn or npm install
```

Copy the `.env.example` as `.env` in `frontend` folder
```
On terminal/powershell
cp .env.example .env
```

## Running Backend on Local Server

Activate virtual environment

```
cd backend
source venv/bin/activate
```

(Optional) Run test
```
python manage.py test
```

Then run the server, api endpoint should be available on http://localhost:8000/api

```
python manage.py runserver
```

## Running Frontend on Local Server

Start development server

```
cd frontend
yarn start or npm start
```

Frontend should be available on http://localhost:3000/

### Test User
By default, the database for development server in `backend/db.sqlite3` is already filled with some data for ease of development. The superuser id is `irene` and password is `irene` as well.

If you want to start clean. Delete `db.sqlite3` and follow this step in `backend folder`
```py
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
```

### Changes from Original Rengorum to Posthings

```
ALTER TABLE threads_thread
DROP COLUMN forum_id

DROP TABLE forums_forum

CREATE SEQUENCE userprofile_role_id_seq;
CREATE TABLE accounts_userprofile_role(
    id int NOT NULL DEFAULT nextval('userprofile_role_id_seq'),
    name varchar NOT NULL,
    PRIMARY KEY (id)
);

CREATE SEQUENCE userprofile_address_id_seq;
CREATE TABLE accounts_userprofile_address(
    id int NOT NULL DEFAULT nextval('userprofile_address_id_seq'),
    full_address varchar NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO accounts_userprofile_role (name) VALUES
    ('Administrator'),
    ('Unverified User'),
    ('Verified User');

ALTER TABLE accounts_userprofile
DROP COLUMN status;

ALTER TABLE accounts_userprofile
ADD COLUMN role_id integer,
ADD COLUMN address_id integer;


ALTER TABLE accounts_userprofile_address
DROP COLUMN full_address;

ALTER TABLE accounts_userprofile_address
ADD COLUMN address_line varchar NOT NULL default 'test',
ADD COLUMN province varchar NOT NULL default 'test',
ADD COLUMN city varchar NOT NULL default 'test',
ADD COLUMN postal_code integer NOT NULL DEFAULT 0;

CREATE SEQUENCE user_legal_doc_id_seq;
CREATE TABLE accounts_legal_document (
	id int NOT NULL DEFAULT nextval('user_legal_doc_id_seq'),
	user_id int NOT NULL,
	document varchar NOT NULL
	created_at date,
	updated_at date
);

INSERT INTO accounts_userprofile_role 
	(id, name) VALUES
	(98, 'SUSPENDED'),
	(99, 'BANNED');

CREATE SEQUENCE user_follow_id_seq;
CREATE TABLE accounts_user_follow (
	id int NOT NULL DEFAULT nextval('user_follow_id_seq'),
	user_id int NOT NULL,
	followed_user_id int NOT NULL,
	created_at date,
	updated_at date
);
```
