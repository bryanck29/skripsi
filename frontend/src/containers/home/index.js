import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  fetchThreads,
  createThreadSave,
  createThreadToggle,
  createThread,
} from '../../actions';
import ThreadList from '../../components/threadlist';
import NewThread from '../../components/newthread';

class HomeContainer extends Component {
  componentDidMount() {
    this.props.fetchThreads();
  }

  componentWillReceiveProps(newProps) {
    const {threads: oldThreads} = this.props.match.params;
    const {threads: newThreads} = newProps.match.params;
    if (oldThreads !== newThreads) {
      this.setState({threads: newThreads});
    }
  }

  handleNewThread = () => {
    this.props.fetchThreads();
  }

  render() {
    const {
      isAuthenticated,
      newThreadLoading,
      newThreadSuccess,
      newThreadName,
      newThreadContent,
      newThreadId,
      newThreadError,
      newThreadShow,
      createThread,
      createThreadSave,
      createThreadToggle,
    } = this.props;

    return (
      <div>
        <NewThread
            isAuthenticated={isAuthenticated}
            isLoading={newThreadLoading}
            success={newThreadSuccess}
            name={newThreadName}
            content={newThreadContent}
            id={newThreadId}
            error={newThreadError}
            showEditor={newThreadShow}
            createThread={createThread}
            updateNewThread={createThreadSave}
            toggleShowEditor={createThreadToggle}
            maxLength={2000}
            handleNewThread={this.handleNewThread}
        />
        <ThreadList {...this.props} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.home.isLoading,
  threads: state.home.threads,
  error: state.home.error,
  isAuthenticated: state.auth.isAuthenticated,
  newThreadLoading: state.home.newThreadLoading,
  newThreadSuccess: state.home.newThreadSuccess,
  newThreadName: state.home.newThreadName,
  newThreadContent: state.home.newThreadContent,
  newThreadId: state.home.newThreadId,
  newThreadError: state.home.newThreadError,
  newThreadShow: state.home.newThreadShow,
});

const mapDispatchToProps = dispatch => ({
  fetchThreads: () => {
    dispatch(fetchThreads());
  },
  createThread: newThread => {
    dispatch(createThread(newThread));
  },
  createThreadSave: newThread => {
    dispatch(createThreadSave(newThread));
  },
  createThreadToggle: () => {
    dispatch(createThreadToggle());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeContainer);
