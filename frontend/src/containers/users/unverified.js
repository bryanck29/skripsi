import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchUnverifiedUsers} from '../../actions';
import UserList from '../../components/userlist';

class UnverifiedUsersContainer extends Component {
  componentDidMount() {
    this.props.fetchUnverifiedUsers();
  }

  render() {
    return <UserList {...this.props} />;
  }
}

const mapStateToProps = state => ({
  isLoading: state.users.isLoading,
  users: state.users.users,
  error: state.users.error,
});

const mapDispatchToProps = dispatch => ({
  fetchUnverifiedUsers: () => {
    dispatch(fetchUnverifiedUsers());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UnverifiedUsersContainer);
