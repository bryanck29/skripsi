import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchVerifiedUsers} from '../../actions';
import UserList from '../../components/userlist';

class VerifiedUsersContainer extends Component {
  componentDidMount() {
    this.props.fetchVerifiedUsers();
  }

  render() {
    return <UserList {...this.props} />;
  }
}

const mapStateToProps = state => ({
  isLoading: state.users.isLoading,
  users: state.users.users,
  error: state.users.error,
});

const mapDispatchToProps = dispatch => ({
  fetchVerifiedUsers: () => {
    dispatch(fetchVerifiedUsers());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VerifiedUsersContainer);
