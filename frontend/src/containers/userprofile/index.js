import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchUserProfile, checkFollowUser} from '../../actions';
import StatusMessage from '../../components/statusmessage';
import Profile from '../../components/profile';
import './styles.css';

class UserProfileContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      followFlag: null,
    };
  }
  
  componentDidMount() {
    const {username} = this.props.match.params;
    this.props.fetchUserProfile(username);
    this.props.checkFollowUser(this.props.authUsername, username);
  }

  componentWillReceiveProps(newProps) {
    const {username: oldUsername} = this.props.match.params;
    const {username: futureUsername} = newProps.match.params;
    if(newProps.followFlag != null && newProps.followFlag != undefined){
      //this.state.followFlag = newProps.followFlag;
      this.setState({followFlag: newProps.followFlag});
    }
    if (oldUsername !== futureUsername) {
      this.props.fetchUserProfile(futureUsername);
    }
  }

  render() {
    const {isLoading, error, profile, authUsername, isAuthenticated} = this.props;
    if (error || !profile || isLoading) {
      return (
        <StatusMessage
          error={error || !profile}
          errorClassName="userProfile-error"
          errorMessage={error}
          loading={isLoading}
          loadingMessage={`We are fetching the user profile for you`}
          type="default"
        />
      );
    }

    const {
      name,
      username,
      role,
      bio,
      avatar,
      is_staff,
      date_joined,
    } = profile;

    return (
      <Profile
        username={username}
        name={name}
        role={role}
        avatar={avatar}
        bio={bio}
        dateJoined={date_joined}
        isStaff={is_staff}
        authUsername={authUsername}
        isAuthenticated={isAuthenticated}
        checkFollowUser={checkFollowUser}
        isFollowed={this.state.followFlag}
      />
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.userProfile.isLoading,
  profile: state.userProfile.profile,
  error: state.userProfile.error,
  isAuthenticated: state.auth.isAuthenticated,
  authUsername: state.auth.username,
  followFlag: state.userProfile.followFlag,
});

const mapDispatchToProps = dispatch => ({
  fetchUserProfile: username => {
    dispatch(fetchUserProfile(username));
  },
  checkFollowUser: (user, target_user) => {
    dispatch(checkFollowUser(user, target_user));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserProfileContainer);
