 import React, {Component} from 'react';
 import {connect} from 'react-redux';
 import Register from '../../components/register';
 import Modal from '../../components/modal';
 import {hideModal, registerReset, showModal, register} from '../../actions';

 class AddressModal extends Component {
   componentWillMount() {
     if (this.props.isAuthenticated) {
       this.props.handleClose();
     }
   }

  render() {
    const {
      isAuthenticated,
      isLoading,
      error,
      handleRegister,
      showLogin,
      handleClose,
      showPrev,
    } = this.props;

    return isAuthenticated ? null : (
      <Modal onClose={handleClose}>
        <Register
          handleRegister={handleRegister}
          showLogin={showLogin}
          isLoading={isLoading}
          showPrev={showPrev}
          error={error}
        />
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.register.error,
  isLoading: state.register.isLoading,
});

const mapDispatchToProps = dispatch => ({
  handleRegister: data => {
    dispatch(register(data));
  },
  handleClose: () => {
    dispatch(hideModal());
    dispatch(registerReset());
  },
  showLogin: () => {
    dispatch(showModal('LOGIN', {}));
    dispatch(registerReset());
  },
  showPrev:()=>{
    dispatch(showModal('REGISTER',{}));
  }
  
  
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddressModal);

// import React, {Component} from 'react';
// import {connect} from 'react-redux';
// import Register from '../../components/register';
// import Modal from '../../components/modal';
// import {hideModal, registerReset, showModal, register} from '../../actions';

// class RegisterModal extends Component {
//   componentWillMount() {
//     if (this.props.isAuthenticated) {
//       this.props.handleClose();
//     }
//   }

//   render() {
//     const {
//       isAuthenticated,
//       isLoading,
//       error,
//       handleRegister,
//       showLogin,
//       handleClose,
//       showNext,
//     } = this.props;

//     return isAuthenticated ? null : (
//       <Modal onClose={handleClose}>
//         <Register
//           handleRegister={handleRegister}
//           showLogin={showLogin}
//           isLoading={isLoading}
//           showNext={showNext}
//           error={error}
//         />
//       </Modal>
//     );
//   }
// }

// const mapStateToProps = state => ({
//   isAuthenticated: state.auth.isAuthenticated,
//   error: state.register.error,
//   isLoading: state.register.isLoading,
// });

// const mapDispatchToProps = dispatch => ({
//   handleRegister: data => {
//     dispatch(register(data));
//   },
//   handleClose: () => {
//     dispatch(hideModal());
//     dispatch(registerReset());
//   },
//   showLogin: () => {
//     dispatch(showModal('LOGIN', {}));
//     dispatch(registerReset());
//   },
//   showNext:()=>{
//     dispatch(showModal('LOGIN', {}));
//     dispatch(registerReset());
//   }
// });

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps,
// )(RegisterModal);
