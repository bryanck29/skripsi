import React from 'react';
import {connect} from 'react-redux';
import RegisterModal from './register';
import LoginModal from './login';
import AddressModal from './address'
import EditProfileModal from './editprofile';

const ModalContainer = props => {
  switch (props.modalType) {
    case 'REGISTER':
      return <RegisterModal callBack = {props.callBack}/>;
    case 'LOGIN':
      return <LoginModal authUsername = {props.authUsername} callBack = {props.callBack}/>;
    case 'EDIT_PROFILE':
      return <EditProfileModal />;
    case 'ADDRESS':
      return <AddressModal/>;
    default:
      return null;
  }
};

const mapStateToProps = state => ({
  modalType: state.modal.modalType,
  modalProps: state.modal.modalProps, // for future use if need to pass props

});

export default connect(mapStateToProps)(ModalContainer);
