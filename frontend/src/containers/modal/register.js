import React, {Component} from 'react';
import {connect} from 'react-redux';
import Register from '../../components/register';
import Modal from '../../components/modal';
import {hideModal, registerReset, showModal, register} from '../../actions';

class RegisterModal extends Component {
  componentWillMount() {
    if (this.props.isAuthenticated) {
      this.props.handleClose();
    }
  }

  render() {
    const {
      isAuthenticated,
      isLoading,
      error,
      handleRegister,
      showLogin,
      handleClose,
      showNext,
      callBack,
    } = this.props;

    return isAuthenticated ? null : (
      <Modal onClose={handleClose}>
        <Register
          handleRegister={handleRegister}
          showLogin={showLogin}
          isLoading={isLoading}
          showNext={showNext}
          error={error}
          callBack={this.props.callBack}
        />
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.register.error,
  isLoading: state.register.isLoading,
});

const mapDispatchToProps = dispatch => ({
  handleRegister: (data, callBack) => {
    dispatch(register(data, callBack));
  },
  handleClose: () => {
    dispatch(hideModal());
    dispatch(registerReset());
  },
  showLogin: () => {
    dispatch(showModal('LOGIN', {}));
    dispatch(registerReset());
  },
  showNext:()=>{
    dispatch(showModal('ADDRESS', {}));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterModal);
