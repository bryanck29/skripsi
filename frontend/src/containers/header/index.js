import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Navlink from '../../components/navlink';
import UserMenu from '../../components/usermenu';
import './styles.css';
import {showModal, logout} from '../../actions';
import {Search} from 'semantic-ui-react';
import _ from 'lodash';
import {searchUsers} from '../../actions';

const initialState = { isLoading: false, results: [], value: '' }

class HeaderContainer extends Component {
  state = initialState

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value })
    setTimeout(() => {
      if (this.state.value.length < 1) return this.setState(initialState)
      this.props.handleSearchUsers(this.state.value)
    }, 300)
    this.setState({
      isLoading: false,
      results: this.props.users,
    })
  }

  handleresultRenderer = (data) => {
    return <Link to={ `/user/${data.username}` } > {data.username} </Link>
  };

  render() {
    const {
      isAuthenticated,
      username,
      name,
      avatar,
      handleLogout,
      isLoading,
      showRegister,
      showLogin,
      showAddress,
      showEditProfile,
    } = this.props;

    return (
      <div className="headerContainer">
        <div className="leftSide">
          <Navlink />
          <Search
            loading={isLoading}
            onResultSelect={this.handleResultSelect}
            onSearchChange={_.debounce(this.handleSearchChange, 500, {
              leading: true,
            })}
            results={this.state.results}
            value={this.state.value}
            resultRenderer={this.handleresultRenderer}
            onResultSelect={this.handleResultSelect}
          />
          
        </div>
        <UserMenu
          isAuthenticated={isAuthenticated}
          username={username}
          name={name}
          avatar={avatar}
          logout={handleLogout}
          isLoading={isLoading}
          showRegister={showRegister}
          showLogin={showLogin}
          showAddress={showAddress}
          showEditProfile={showEditProfile}
          callBack={this.callBack}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  username: state.auth.username,
  name: state.auth.name,
  avatar: state.auth.avatar,
  isAuthenticated: state.auth.isAuthenticated,
  isLoading: state.auth.isLoading,
  users: state.users.users,
});

const mapDispatchToProps = dispatch => ({
  handleLogout: () => {
    dispatch(logout());
  },
  showRegister: () => {
    dispatch(showModal('REGISTER', {}));
  },
  showLogin: () => {
    dispatch(showModal('LOGIN', {}));
  },
  showEditProfile: () => {
    dispatch(showModal('EDIT_PROFILE', {}));
  },
  showAddress:()=>{
    dispatch(showModal('ADDRESS',{}));
  },
  handleSearchUsers: (kwarg) => {
    dispatch(searchUsers(kwarg));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HeaderContainer);
