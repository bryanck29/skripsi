import axios from 'axios';
import {
  USER_LOGIN_URL,
  USER_LOGOUT_URL,
  USER_REGISTER_URL,
  USER_EDIT_URL,
  USER_URL,
  UNVERIFIED_USER_URL,
  VERIFIED_USER_URL,
  USER_SEARCH_URL,
  USER_VERIFY_URL,
  USER_BANNED_URL,
  USER_FOLLOW_URL,
  USER_UNFOLLOW_URL,
  USER_CHECK_FOLLOW_URL,
  USER_REPORT_URL,
} from './constants';
import {getConfig} from '../utils/config';

export const loginApi = (username, password) => {
  return axios.post(USER_LOGIN_URL, {username, password}, getConfig());
};

export const logoutApi = () => {
  return axios.post(USER_LOGOUT_URL, null, getConfig());
};

export const registerApi = data => {
  return axios.post(USER_REGISTER_URL, data, getConfig());
};

export const fetchUserProfileApi = username => {
  return axios.get(USER_URL + username, getConfig());
};

export const editProfileApi = (username, newProfile) => {
  return axios.put(
    USER_URL + username + USER_EDIT_URL,
    newProfile,
    getConfig(),
  );
};

export const verifyUserApi = username => {
  return axios.put(
    USER_URL + username + USER_VERIFY_URL,
    username = {username: username},
    getConfig(),
  );
};

export const bannedUserApi = username => {
  return axios.put(
    USER_URL + username + USER_BANNED_URL,
    username = {username: username},
    getConfig(),
  );
};

export const fetchUsersApi = () => {
  return axios.get(USER_URL, getConfig());
};

export const fetchUnverifiedUsersApi = () => {
  return axios.get(UNVERIFIED_USER_URL, getConfig());
};

export const fetchVerifiedUsersApi = () => {
  return axios.get(VERIFIED_USER_URL, getConfig());
};

export const searchUsersApi = (username) => {
  return axios.get(USER_SEARCH_URL + username, getConfig());
};

export const followUserApi = (user, followed_user) => {
  return axios.post(
    USER_URL + followed_user + USER_FOLLOW_URL,
    user = {user: user, followed_user: followed_user},
    getConfig(),
  );
};

export const unfollowUserApi = (user, followed_user) => {
  return axios.post(
    USER_URL + followed_user + USER_UNFOLLOW_URL,
    user = {user: user, followed_user: followed_user},
    getConfig(),
  );
};

export const checkFollowUserApi = (user, target_user) => {
  return axios.post(
    USER_URL + target_user + USER_CHECK_FOLLOW_URL,
    user = {user: user, followed_user: target_user},
    getConfig(),
  );
};

export const reportUserApi = (user, reported_user) => {
  return axios.post(
    USER_URL + reported_user + USER_REPORT_URL,
    user = {reporter: user, reported_user: reported_user},
    getConfig(),
  );
};