import {
  FETCH_HOME_REQUEST,
  FETCH_HOME_SUCCESS,
  FETCH_HOME_FAILURE,
  CREATE_THREAD_REQUEST,
  CREATE_THREAD_SUCCESS,
  CREATE_THREAD_FAILURE,
  CREATE_THREAD_SAVE,
  CREATE_THREAD_TOGGLE,
  LOGOUT,
} from '../actions/types';

const initialState = {
  isLoading: false,
  threads: null,
  error: null,
  ...newThreadInitialState,

};

const newThreadInitialState = {
  newThreadLoading: false,
  newThreadSuccess: false,
  newThreadName: '',
  newThreadContent: '',
  newThreadId: null,
  newThreadError: null,
  newThreadShow: false,
};

const home = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_HOME_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case FETCH_HOME_SUCCESS:
      return {
        isLoading: false,
        threads: action.threads,
        error: null,
      };
    case FETCH_HOME_FAILURE:
      return {
        ...initialState,
        error: action.error,
      };
    case CREATE_THREAD_REQUEST:
      return {
        ...state,
        newThreadLoading: true,
        newThreadSuccess: false,
        newThreadError: null,
        newThreadName: action.newThread.name,
        newThreadContent: action.newThread.content,
      };
    case CREATE_THREAD_SUCCESS:
      return {
        ...state,
        newThreadLoading: false,
        newThreadSuccess: true,
        newThreadName: '',
        newThreadContent: '',
        newThreadId: action.newThread.id,
        newThreadShow: false,
        newThreadError: null,
      };
    case CREATE_THREAD_FAILURE:
      return {
        ...state,
        newThreadLoading: false,
        newThreadSuccess: false,
        newThreadId: null,
        newThreadShow: true,
        newThreadError: action.error,
      };
    case CREATE_THREAD_SAVE:
      return {
        ...state,
        newThreadName: action.name,
        newThreadContent: action.content,
      };
    case CREATE_THREAD_TOGGLE:
      return {
        ...state,
        newThreadShow: !state.newThreadShow,
        newThreadSuccess: false,
        newThreadError: null,
      };
    case LOGOUT:
      return {
        ...state,
        ...newThreadInitialState,
      };
    default:
      return state;
  }
};

export default home;
