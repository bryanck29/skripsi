import {
  FETCH_USER_PROFILE_REQUEST,
  FETCH_USER_PROFILE_SUCCESS,
  FETCH_USER_PROFILE_FAILURE,
  CHECK_FOLLOW_USER_REQUEST,
  CHECK_FOLLOW_USER_SUCCESS,
  CHECK_FOLLOW_USER_FAILURE,
} from '../actions/types';

const initialState = {
  isLoading: false,
  profile: null,
  error: null,
  followFlag: null,
};

const userProfile = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_PROFILE_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case FETCH_USER_PROFILE_SUCCESS:
      return {
        isLoading: false,
        profile: action.profile,
        error: null,
      };
    case FETCH_USER_PROFILE_FAILURE:
      return {
        ...initialState,
        error: action.error,
      };
    case CHECK_FOLLOW_USER_REQUEST:
      return {
        followFlag: null,
        error: null,
      };
    case CHECK_FOLLOW_USER_SUCCESS:
      return {
        isLoading: false,
        followFlag: action.followFlag,
        error: null,
      };
    case CHECK_FOLLOW_USER_FAILURE:
      var followFlag = null;
      if(action.error == "Not Followed"){
        followFlag = false;
      }
      return {
        followFlag: followFlag,
        error: action.error,
      };
    default:
      return state;
  }
};

export default userProfile;
