import {
  FETCH_USER_PROFILE_REQUEST,
  FETCH_USER_PROFILE_SUCCESS,
  FETCH_USER_PROFILE_FAILURE,
  CHECK_FOLLOW_USER_REQUEST,
  CHECK_FOLLOW_USER_SUCCESS,
  CHECK_FOLLOW_USER_FAILURE,
} from './types';
import {fetchUserProfileApi, checkFollowUserApi,} from '../api';
import {apiErrorHandler} from '../utils/errorhandler';

export const fetchUserProfile = username => dispatch => {
  dispatch(fetchUserProfileRequest());

  fetchUserProfileApi(username)
    .then(response => {
      dispatch(fetchUserProfileSuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(fetchUserProfileFailure(errorMessage));
    });
};

export const checkFollowUser = (username, targetUser) => dispatch => {
  dispatch(checkFollowUserRequest());
  checkFollowUserApi(username, targetUser)
    .then(response => {
      dispatch(checkFollowUserSuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(checkFollowUserFailure(errorMessage));
    });
};

export const fetchUserProfileRequest = () => {
  return {
    type: FETCH_USER_PROFILE_REQUEST,
  };
};

export const fetchUserProfileSuccess = data => {
  return {
    type: FETCH_USER_PROFILE_SUCCESS,
    profile: data,
  };
};

export const fetchUserProfileFailure = error => {
  return {
    type: FETCH_USER_PROFILE_FAILURE,
    error,
  };
};

export const checkFollowUserRequest = () => {
  return {
    type: CHECK_FOLLOW_USER_REQUEST,
  };
};

export const checkFollowUserSuccess = data => {
  return {
    type: CHECK_FOLLOW_USER_SUCCESS,
    followFlag: data.isFollowed,
  };
};

export const checkFollowUserFailure = error => {
  return {
    type: CHECK_FOLLOW_USER_FAILURE,
    error,
  };
};
