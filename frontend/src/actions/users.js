import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  SEARCH_USERS_SUCCESS,
  FOLLOW_USER_SUCCESS,
  UNFOLLOW_USER_SUCCESS,
  FOLLOW_USER_FAILURE,
  UNFOLLOW_USER_FAILURE,
} from './types';
import {
  fetchUsersApi, 
  searchUsersApi,
} from '../api';
import {apiErrorHandler} from '../utils/errorhandler';

export const fetchUsers = () => dispatch => {
  dispatch(fetchUsersRequest());

  fetchUsersApi()
    .then(response => {
      dispatch(fetchUsersSuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(fetchUsersFailure(errorMessage));
    });
};

export const searchUsers = (kwarg) => dispatch => {

  searchUsersApi(kwarg)
    .then(response => {
      dispatch(searchUsersSuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      return errorMessage;
    });
};

export const fetchUsersRequest = () => {
  return {
    type: FETCH_USERS_REQUEST,
  };
};

export const fetchUsersSuccess = data => {
  return {
    type: FETCH_USERS_SUCCESS,
    users: data,
  };
};

export const fetchUsersFailure = error => {
  return {
    type: FETCH_USERS_FAILURE,
    error,
  };
};

export const searchUsersSuccess = data => {
  return {
    type: SEARCH_USERS_SUCCESS,
    users: data,
  };
};

/* export const followUserSuccess = data => {
  return {
    type: FOLLOW_USER_SUCCESS,
    users: data,
  };
};

export const followUserFailure = data => {
  return {
    type: UNFOLLOW_USER_FAILURE,
    users: data,
  };
};

export const unfollowUserSuccess = data => {
  return {
    type: UNFOLLOW_USER_SUCCESS,
    users: data,
  };
};

export const unfollowUserFailure = data => {
  return {
    type: UNFOLLOW_USER_FAILURE,
    users: data,
  };
}; */
