import React, { Component } from 'react'
import {Form, Icon, Message, Button} from 'semantic-ui-react';
import './styles.css';

export class AddressForm extends Component {

    continue = e => {
        e.preventDefault();

        this.props.nextStep();
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep();

    }
    
    render() {
        let {values, handleChange, handleSubmit, showLogin} = this.props;//jangan lupa declare variable handle change nya
        return (
          <div>
            <Message
              attached
              header="Address Form"
              content="Please fill out the address form"
            />
        
          <Form className="attached fluid segment">
                
            <Form.Input
            required
            label="Address"
            placeholder="Address"
            type="address"
            name="address"
            value = {values.address}
            defaultValue = {values.address}
            onChange = {handleChange}
          />

          <Form.Input
            required
            label="Province"
            placeholder="Province"
            type="province"
            name="province"
            value = {values.province}
            defaulValue = {values.province}
            onChange = {handleChange}
          />

            <Form.Input
            required
            label="City"
            placeholder="City"
            type="city"
            name="city"
            value = {values.city}
            defaultValue = {values.city}
            onChange = {handleChange}
          />

          <Form.Input
            required
            label="Postalcode"
            placeholder="Postal Code"
            type="postalcode"
            name="postalcode"
            value = {values.postalcode}
            defaultValue = {values.postalcode}
            onChange = {handleChange}
          />
        <Button
            color="blue"
            onClick={handleSubmit}>
            Submit
          </Button>
                <Button
                color="blue"
                onClick = {this.back}
          >
            Back
          </Button>
        </Form>
        <Message attached="bottom" warning>
            <Icon name="help" />
            Already signed up?&nbsp;
            {/* eslint-disable-next-line */}
            <a className="register-login" onClick={showLogin}>
              Login here
            </a>
            &nbsp;instead.
        </Message>
        </div>
            
        )
    }
}

export default AddressForm
