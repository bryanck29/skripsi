import React, {Component} from 'react';
import {Form, Icon, Message, Button} from 'semantic-ui-react';
import StatusMessage from '../../components/statusmessage';
import {imageUploadApi} from '../../api/image';
import AddressForm from './AddressForm';
import UserForm from './UserForm';
import axios from 'axios';
import './styles.css';


export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      username: '',
      name: '',
      first_name:'',
      last_name:'',
      email: '',
      password: '',
      selectedFile:null,
      selectedError:null,
      fileUpload:false,
      checked: true,
      address : '',
      postalcode : '',
      province : '',
      city:'',
      file:null,
    };
  }

  //coba next step

  nextStep = () => {
    const {step} = this.state;
    this.setState({
      step: step + 1 
    });
  }

//coba prev step

prevStep = () => {
  const {step} = this.state;
  this.setState({
    step: step - 1 
  });
}

//coba handle field change (beda sama handleChange jadi gw bikin handleChange2 ok)

  handleChange = (e, {name, value}) => {
    this.setState({[name]: value});
  };

  fileSelectedHandler = files =>{
    this.setState({
      selectedFile: files[0],
    })
  }

  fileUploadHandler = () =>{
    const selectedFile = this.state;
    imageUploadApi(this.state.selectedFile)
    .then(response => {
      console.log(response);
      this.setState({
        file: response.data.secure_url,
        fileUpload: false,
      });
      this.fileSelectedHandler();
    })
    .catch(error => {
      console.log(error);
      this.setState({
        selectedError: 'Image Upload Error',
        selectedFile: null,
        fileUpload: false,
      });
    });
  }


  handleCheckbox = () => {
    this.setState({checked: !this.state.checked});
  };

  isFormValid = () => {
    const {username, name, email, password, checked,first_name,last_name,address,postalcode,province,city} = this.state;

    let isFormValid = true;
    if (!username || !email || !password || !checked || !first_name ||!last_name||!address||!postalcode||!province||!city) {
      isFormValid = false;
    }
    return isFormValid;
  };

  handleSubmit = e => {
    const selectedFile = this.state.selectedFile;
    imageUploadApi(selectedFile)
    .then(response => { 
      this.setState({
        file: response.data.secure_url,
        fileUpload: false,
      });
      //if (this.isFormValid()) {
        let data = {
          username: this.state.username,
          email: this.state.email,
          password: this.state.password,
          first_name:this.state.first_name,
          last_name:this.state.last_name,
          address: this.state.address,
          postal_code: this.state.postalcode,
          province: this.state.province,
          city: this.state.city,
          document: this.state.file,
        };
        this.props.handleRegister(data, this.props.callBack);
      //}
    })
    .catch(error => {
      console.log(error);
      this.setState({
        selectedError: 'Image Upload Error',
        selectedFile: null,
        fileUpload: false,
      });
    });
  };

  render() {
    let {isLoading, error, showLogin} = this.props;
    //const buat nyoba
    //declare state
    const step = this.state.step;
    //const {address,postalcode,province,city} = this.state;
    //declare state jg
    //const values = {username,name,first_name,last_name,email,password,file,selectedfile,selectedError,fileUpload,checked,address, postalcode, province, city}
    const values = this.state;

    const statusMessage = (
      <StatusMessage
        error={error}
        errorMessage={error || 'Login Error'}
        loading={isLoading}
        loadingMessage={'Registering your account'}
        type="modal"
      />
    );


    // buat ganti ganti form jadi case 1 case 2 dst
    switch(this.state.step){
      case 1:    
        return (
          <UserForm
              nextStep = {this.nextStep}
              handleChange = {this.handleChange}
              handleCheckbox = {this.handleCheckbox}
              fileSelectedHandler = {this.fileSelectedHandler}
              statusMessage = {statusMessage}
              showLogin = {showLogin}
              values = {values}
            />

        );
      case 2:
        //ini bakal diisi ama address form
        return (
          <AddressForm
              nextStep = {this.nextStep}
              prevStep = {this.prevStep}
              handleChange = {this.handleChange}
              handleSubmit = {this.handleSubmit}
              showLogin = {showLogin}
              values = {values}
            />

        );
    }
  }
}
