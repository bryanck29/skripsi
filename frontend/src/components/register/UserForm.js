import React, { Component } from 'react'
import {Form, Icon, Message, Button} from 'semantic-ui-react';
import Dropzone from 'react-dropzone';
import './styles.css';

export class UserForm extends Component {

    continue = e => {
        e.preventDefault();

        this.props.nextStep();
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep();

    }
    
    render() {
      let { values, handleChange, showLogin, fileSelectedHandler } = this.props;
      return (
        <div>
          <Message
            attached
            header="Welcome to our site!"
            content="Fill out the form below to sign-up for a new account"
          />
          {this.statusMessage}
          <Form className="attached fluid segment">
            <Form.Input
              required
              label="Username"
              placeholder="Username"
              type="text"
              name="username"
              value = {values.username}
              defaultValue = {values.username}
              onChange={handleChange}
            />
            <Form.Input
              required
              label = "First Name"
              placeholder = "First Name"
              type = "text"
              name = "first_name"
              value = {values.first_name}
              defaultValue = {values.first_name}
              onChange={handleChange}
            />
            <Form.Input
              required
              label = "Last Name"
              placeholder = "Last Name"
              type = "text"
              name = "last_name"
              value = {values.last_name}
              defaultValue = {values.last_name}
              onChange={handleChange}
            />

            <Form.Input
              required
              label="Email"
              placeholder="Email"
              type="email"
              name="email"
              value = {values.email}
              defaultValue = {values.email}
              onChange={handleChange}
            />
            <Form.Input
              required
              label="Password"
              type="password"
              name="password"
              value = {values.password}
              defaultValue = {values.password}
              onChange={handleChange}
            />
          <div className = "upload-image">
              <label><b>Legal Document</b></label>
            <Dropzone
              required
              name="selectedFile"
              type="selectedFile"
              onDrop={fileSelectedHandler}
              multiple = {false}
            >
            </Dropzone>
          </div>

            <Form.Checkbox
              inline
              required
              label="I agree to the terms and conditions"
              name="agreement"
              values = {values.checked}
              onChange={this.handleCheckbox}
              defaultValue = {values.checked}
            />
            
            <Button
              color="blue"
              onClick = {this.continue}
              >
              Continue
            </Button>



          </Form>
          <Message attached="bottom" warning>
            <Icon name="help" />
            Already signed up?&nbsp;
            {/* eslint-disable-next-line */}
            <a className="register-login" onClick={showLogin}>
              Login here
            </a>
            &nbsp;instead.
          </Message>

        </div>
      );
    }
        
}

export default UserForm
