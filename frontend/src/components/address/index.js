import React, {Component} from 'react';
import {Form, Icon, Message, Button} from 'semantic-ui-react';
import StatusMessage from '../../components/statusmessage';
import './styles.css';


export default class Address extends Component{
    constructor(props) {
        super(props);
        const file = this.props;
        this.state = {
          address: '',
          file:file,
        };
}

  handleChange = (e, {address, value}) => {
    this.setState({[address]: value});
  };



  isFormValid = () => {
    const {address} = this.state;
    let isFormValid = true;
    if (!address) {
      isFormValid = false;
    }
    return isFormValid;
  };


  handleSubmit = e => {
    if (this.isFormValid()) {
      let data = {
        address:this.address,
      };
      this.props.handleRegister(data);
    }
  };

  // render() {
  //   let {isLoading, error, showRegister} = this.props;

  //   const statusMessage = (
  //     <StatusMessage
  //       error={error}
  //       errorMessage={error || 'Login Error'}
  //       loading={isLoading}
  //       loadingMessage={'Signing in'}
  //       type="modal"
  //     />
  //   );

  render() {
    let {isLoading, error, showRegister} = this.props;
    const statusMessage = (
      <StatusMessage
        error={error}
        errorMessage={error || 'Login Error'}
        loading={isLoading}
        loadingMessage={'Registering your account'}
        type="modal"
      />
    );


//     return (
//       <div>
//         <Message attached header="Login" />
//         {statusMessage}
//         <Form className="attached fluid segment">
//           <Form.Input
//             required
//             label="Username"
//             placeholder="Username"
//             type="text"
//             name="username"
//             value={this.state.username}
//             onChange={this.handleChange}
//           />
//           <Form.Input
//             required
//             label="Password"
//             type="password"
//             name="password"
//             value={this.state.password}
//             onChange={this.handleChange}
//           />
//           <Button
//             color="blue"
//             loading={isLoading}
//             disabled={isLoading}
//             onClick={this.handleSubmit}>
//             Login
//           </Button>
          
//         </Form>
//         <Message attached="bottom" warning>
//           <Icon name="help" />
//           New to this site?&nbsp;
//           {/* eslint-disable-next-line */}
//           <a className="login-register" onClick={showRegister}>
//             Register here
//           </a>
//           &nbsp;instead.
//         </Message>
//       </div>
//     );
//   }
// }

  return (
    <div>
      {statusMessage}
      <Form className="attached fluid segment">
        <Form.Input
          required
          label="Address"
          placeholder="Address"
          type="text"
          name="address"
          value={this.state.address}
          onChange={this.handleChange}
        />
        <Button
          color="blue"
          loading={isLoading}
          disabled={isLoading}
          onClick={showRegister}>
          Prev
        </Button>
      </Form>
    </div>
  );
}
}