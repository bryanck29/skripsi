import React, {Component} from 'react';
import Avatar from '../avatar';
import './styles.css';
import {
  verifyUserApi, 
  bannedUserApi, 
  followUserApi,
  unfollowUserApi,
  reportUserApi,
} from '../../api/user';
class Profile extends Component {
  /* componentDidMount() {
    console.log(this.props);
  }

  componentWillReceiveProps(newProps) {
    console.log(newProps);
    
  }*/

  formatDateTime(datetime) {
    return datetime.split('.')[0].replace('T', ' ');
  }

  handleVerify = username => {
    verifyUserApi(username);
  //  this.props.fetchUserProfile(username);
  }  
  
  handleBanned = username => {
    bannedUserApi(username);
    alert("User has been banned!");
  //  this.props.fetchUserProfile(username);
  }

  handleFollow = username => {
    followUserApi(this.props.authUsername, username);
    this.props.checkFollowUser(this.props.authUsername, username);
    alert("You have successfully followed this user!");
    this.forceUpdate(console.log(this.props));
  }

  handleUnfollow = username => {
    unfollowUserApi(this.props.authUsername, username);
    this.props.checkFollowUser(this.props.authUsername, username);
    alert("You have unfollowed this user!");
    this.forceUpdate();
  }

  handleReport = username => {
    reportUserApi(this.props.authUsername, username);
    alert("Your report has been submitted!");
    this.forceUpdate();
  }

  render() {
    const {
      name,
      username,
      role,
      avatar,
      bio,
      isStaff,
      dateJoined,
      isFollowed,
    } = this.props;

    let button = null;
    if(role === "Unverified User"){
      //if(isStaff === true){
        button = <button className="positive ui button" onClick = {this.handleVerify.bind(null,username)}>Verify</button>
      //}
    }
    else if(role === "Verified User"){
      //if(isStaff === true){
        button = <div><button className="negative ui button" onClick = {this.handleBanned.bind(null,username)}>Banned</button></div>
      //}
      //else{
        if(isFollowed === false){
          button = <div>
          <button className="ui active button" onClick = {this.handleFollow.bind(null,username)}>Follow</button>
          <button className="negative ui button" onClick = {this.handleReport.bind(null,username)}>Report</button>
          </div>
        }
        else if(isFollowed === true){
          button = <div>
          <button className="ui active button" onClick = {this.handleUnfollow.bind(null,username)}>Unfollow</button>
          <button className="negative ui button" onClick = {this.handleReport.bind(null,username)}>Report</button>
          </div>
        }
        
      //}
    }

      return (
        <div className="profileContainer">
          <div>
            <Avatar className="profileAvatar" avatar={avatar} centered={false} />
          </div>
          <div className="profileInfo">
            <div className="name">{name}</div>
            <div className="username">
              <strong>@{username}</strong>
              <b className="staffStatus">{isStaff ? ' (Staff) ' : ''}</b>
            </div>
            <div className="role">
              <strong>Status: </strong>
              {role}
            </div>
            <div className="dateJoined">
              <strong>Joined: </strong>
              {dateJoined}
            </div>
            <div className="bio">
              <strong>Bio: </strong>
              {bio}
            </div>
            {button}
          </div>
        </div>
      );
  }
}

export default Profile;
