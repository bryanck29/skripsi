import React from 'react';
import logo from './logo.svg';
import './styles.css';

const Logo = () => {
  return (
    <div className="logoContainer">
      <div className="logoTitle">Posthings</div>
    </div>
  );
};

export default Logo;
