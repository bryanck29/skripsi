import React from 'react';
import Logo from '../logo';
import {Link} from 'react-router-dom';
import {Icon} from 'semantic-ui-react';
import {Dropdown, Search} from 'semantic-ui-react';
import './styles.css';

const Navlink = () => {
  return (
    <div className="navlinkContainer">
      <Logo />
      <div className="link">
        <Icon name="home" className="navlinkIcon" />
        <Link style={{color: 'white'}} to="/">Home</Link>
      </div>
      
      <div className="link">
        <Icon name="users" className="navlinkIcon" />
        <label style={{color: 'white'}}> User</label>
        <Dropdown style={{color: 'white'}}>
          <Dropdown.Menu>
            <Dropdown.Item as = {Link} to = "/users">
              All
            </Dropdown.Item>
            <Dropdown.Item as = {Link} to = "/users/unverified">
              Unverified
            </Dropdown.Item>
            <Dropdown.Item as = {Link} to = "/users/verified">
              Verified
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </div>
  );
};

export default Navlink;
