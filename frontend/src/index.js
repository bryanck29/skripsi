import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'semantic-ui-css/semantic.min.css';
import {Provider} from 'react-redux';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import {PersistGate} from 'redux-persist/integration/react';
import Loader from './components/loader';
import store, {persistor} from './store';
import HeaderContainer from './containers/header';
import ModalContainer from './containers/modal';
import UserProfileContainer from './containers/userprofile';
import UsersContainer from './containers/users';
import ThreadContainer from './containers/thread';
import HomeContainer from './containers/home';
import ForumContainer from './containers/forum';
import NotFoundPage from './components/notfoundpage';
import UnverifiedUsersContainer from './containers/users/unverified';
import VerifiedUsersContainer from './containers/users/verified';
import registerServiceWorker from './registerServiceWorker';

//var authUsername  = null;

class App extends React.Component {
  state = {authUsername: null}

  callBack = (username) => {
    this.setState({authUsername: username});
  }

  render() {
    return(
    <Provider store={store}>
    <PersistGate loading={<Loader />} persistor={persistor}>
      <BrowserRouter>
        <Fragment>
          <header className="header-background" />
          <div className="app-layout">
            <HeaderContainer/>
            <Switch>
              <Route path = "/users/unverified" component = {UnverifiedUsersContainer}/>
              <Route path = "/users/verified" component = {VerifiedUsersContainer}/>
              <Route path="/users" component={UsersContainer} />
              <Route path="/user/:username" render={(props) => <UserProfileContainer {...props} />} />
              <Route path="/forum/:forum" component={ForumContainer} />
              <Route path="/thread/:thread" component={ThreadContainer} />
              <Route exact path="/" component={HomeContainer} />
              <Route component={NotFoundPage} />
            </Switch>
          </div>
          <ModalContainer authUsername = {this.state.authUsername} callBack = {this.callBack} />
        </Fragment>
      </BrowserRouter>
    </PersistGate>
  </Provider>)
  }
}

ReactDOM.render(
  <App/>,
  document.getElementById('root'),
);
registerServiceWorker();
