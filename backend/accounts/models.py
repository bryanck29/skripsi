from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token
from django.dispatch import receiver

class UserRole(models.Model):

    ADMINISTRATOR = 1
    UNVERIFIED_USER = 2
    VERIFIED_USER = 3
    SUSPENDED = 98
    BANNED = 99

    """ Model for user profile status """
    name = models.CharField(max_length=32, default='')

    class Meta:
        db_table = "accounts_userprofile_role"

    def __str__(self):
        return self.name

class UserAddress(models.Model):

    """ Model for user profile status """
    address_line = models.CharField(max_length=128, default='')
    province = models.CharField(max_length=32, default='')
    city = models.CharField(max_length=32, default='')
    postal_code = models.IntegerField(default=0)

    class Meta:
        db_table = "accounts_userprofile_address"

    def __str__(self):
        return self.address_line

class UserProfile(models.Model):
    """ Model to represent additional information about user """
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name='profile'
    )
    bio = models.TextField(
        max_length=2000,
        blank=True,
        default=''
    )
    # we use URL instead of imagefield because we'll use 3rd party img hosting later on
    avatar = models.URLField(default='', blank=True)
    role = models.ForeignKey(UserRole, on_delete=models.CASCADE)
    name = models.CharField(max_length=32, default='')
    address = models.ForeignKey(UserAddress, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username

class UserLegalDocument(models.Model):

    user = models.OneToOneField(User, 
        on_delete=models.CASCADE,
        related_name='docs')
    document = models.URLField(default='', blank=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = "accounts_legal_document"

    def __str__(self):
        return "Docs"

class UserFollow(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='follower')
    followed_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='followee')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = "accounts_user_follow"

    def __str__(self):
        return "Follower: " + self.user.username + ", Followee: " + self.followed_user.username

class UserReport(models.Model):

    reporter = models.ForeignKey(User, on_delete=models.CASCADE, related_name='reporter')
    reported_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='reported')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = "accounts_user_report"

    def __str__(self):
        return "Reporter: " + self.reporter.username + ", Reported: " + self.reported_user.username

# automatically create a token for each new user
@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

# New superuser profile
@receiver(post_save, sender=User)
def create_superuser_profile(sender, instance, created, **kwargs):
    if created and instance.is_superuser:
        UserProfile.objects.create(
            user=instance,
            bio='I am the admin and I manage this website',
            avatar='http://res.cloudinary.com/rengorum/image/upload/v1525768360/admin.png',
            role=UserRole.objects.get(id = UserRole.ADMINISTRATOR),
            name='Administrator',
            address=UserAddress.objects.get(id = 1),
        )
