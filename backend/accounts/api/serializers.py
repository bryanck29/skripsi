from rest_framework import serializers
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate
from rest_framework.validators import UniqueValidator
from django.contrib.humanize.templatetags.humanize import naturaltime
from accounts.models import UserProfile
from accounts.models import UserRole
from accounts.models import UserAddress
from accounts.models import UserLegalDocument
from accounts.models import UserFollow
from accounts.models import UserReport
from pprint import pprint
from datetime import date

class UserDetailSerializer(serializers.ModelSerializer):
    bio = serializers.CharField(source='profile.bio')
    avatar = serializers.URLField(source='profile.avatar')
    role = serializers.CharField(source='profile.role')
    name = serializers.CharField(source='profile.name')
    threads = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='thread-detail',
        lookup_field='pk'
    )
    posts = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='post-detail',
        lookup_field='pk'
    )
    date_joined = serializers.SerializerMethodField()
    class Meta:
        model = User
        fields = [
            'username',
            'name',
            'role',
            'bio',
            'avatar',
            'is_staff',
            'date_joined',
            'threads',
            'posts'
        ]
        lookup_field = 'username'

    def get_date_joined(self, obj):
        return naturaltime(obj.date_joined)

class UserListSerializer(serializers.ModelSerializer):
    bio = serializers.CharField(source='profile.bio')
    avatar = serializers.URLField(source='profile.avatar')
    name = serializers.CharField(source='profile.name')
    class Meta:
        model = User
        fields = [
            'username',
            'name',
            'bio',
            'avatar',
            'is_staff',
            'date_joined'
        ]

class UserUpdateSerializer(serializers.ModelSerializer):
    # A field from the user's profile:
    bio = serializers.CharField(source='profile.bio', allow_blank=True)
    name = serializers.CharField(
    	source='profile.name',
    	max_length=32,
    	allow_blank=True
    )
    avatar = serializers.URLField(source='profile.avatar', allow_blank=True)
    current_password = serializers.CharField(
        write_only=True,
        allow_blank=True,
        label=_("Current Password"),
        help_text=_('Required'),
    )
    new_password = serializers.CharField(
        allow_blank=True,
        default='',
        write_only=True,
        min_length=4,
        max_length=32,
        label=_("New Password"),
    )
    email = serializers.EmailField(
        allow_blank=True,
        default='',
        validators=[UniqueValidator(
            queryset=User.objects.all(),
            message='has already been taken by other user'
        )]
    )

    class Meta:
        model = User
        fields = (
            'username',
            'name',
            'email',
            'current_password',
            'new_password',
            'bio',
            'avatar'
        )
        read_only_fields = ('username',)
        lookup_field = 'username'

    def update(self, instance, validated_data):
        # make sure requesting user provide his current password
        # e.g if admin 'endiliey' is updating a user 'donaldtrump',
        # currentPassword must be 'endiliey' password instead of 'donaldtrump' password
        try:
            username = self.context.get('request').user.username
        except:
            msg = _('Must be authenticated')
            raise serializers.ValidationError(msg, code='authorization')

        password = validated_data.get('current_password')
        validated_data.pop('current_password', None)

        if not password:
            msg = _('Must provide current password')
            raise serializers.ValidationError(msg, code='authorization')

        user = authenticate(request=self.context.get('request'),
                            username=username, password=password)
        if not user:
            msg = _('Sorry, the password you entered is incorrect.')
            raise serializers.ValidationError(msg, code='authorization')

        # change password to a new one if it exists
        new_password = validated_data.get('new_password') or None
        if new_password:
            instance.set_password(new_password)
        validated_data.pop('new_password', None)

        # Update user profile fields
        profile_data = validated_data.pop('profile', None)
        profile = instance.profile
        for field, value in profile_data.items():
            if value:
                setattr(profile, field, value)
        # Update user fields
        for field, value in validated_data.items():
            if value:
                setattr(instance, field, value)

        profile.save()
        instance.save()
        return instance

class UserCreateSerializer(serializers.ModelSerializer):
    # A field from the user's profile:
    username = serializers.SlugField(
        min_length=4,
        max_length=32,
        help_text=_(
            'Required. 4-32 characters. Letters, numbers, underscores or hyphens only.'
        ),
        validators=[UniqueValidator(
            queryset=User.objects.all(),
            message='has already been taken by other user'
        )],
        required=True
    )
    password = serializers.CharField(
        min_length=4,
        max_length=32,
        write_only=True,
        help_text=_(
            'Required. 4-32 characters.'
        ),
        required=True
    )
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(
            queryset=User.objects.all(),
            message='has already been taken by other user'
        )]
    )
    bio = serializers.CharField(source='profile.bio', allow_blank=True, default='')
    first_name = serializers.CharField(
        source='profile.first_name',
        default=None,
        max_length=32
    )
    last_name = serializers.CharField(
        source='profile.last_name',
        default=None,
        max_length=32
    )
    address = serializers.CharField(
        source='profile.address.address_line',
        required=True,
        max_length=128
    )
    province = serializers.CharField(
        source='profile.address.province',
        required=True,
        max_length=128
    )
    city = serializers.CharField(
        source='profile.address.city',
        required=True,
        max_length=128
    )
    postal_code = serializers.CharField(
        source='profile.address.postal_code',
        required=True,
        max_length=128
    )
    document = serializers.URLField(source='docs.document')

    class Meta:
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'password',
            'bio',
            'address',
            'province',
            'city',
            'postal_code',
            'document'
        )

    def create(self, validated_data):
        profile_data = validated_data.pop('profile', None)
        username = validated_data['username']
        email = validated_data['email']
        password = validated_data['password']
        first_name = profile_data.get('first_name')
        last_name = profile_data.get('last_name')
        address = UserAddress(
                address_line = profile_data.get('address')['address_line'],
                province = profile_data.get('address')['province'],
                city = profile_data.get('address')['city'],
                postal_code = profile_data.get('address')['postal_code']
        )
        address.save()
        user = User(
                username = username,
                email = email,
                first_name = first_name,
                last_name = last_name
        )
        user.set_password(password)
        user.save()

        legal_docs = UserLegalDocument(
                user = user,
                document = validated_data['docs'].get('document')
        )
        legal_docs.save()

        avatar = profile_data.get('avatar') or None
        if not avatar:
            avatar = 'https://api.adorable.io/avatar/200/' + username
        profile = UserProfile(
            user = user,
            bio = profile_data.get('bio', ''),
            avatar = avatar,
            name = first_name +' '+ last_name,
            role = UserRole.objects.get(id = UserRole.UNVERIFIED_USER),
            address = address
        )
        profile.save()
        return user

class UserTokenSerializer(serializers.Serializer):
    username = serializers.CharField(label=_("Username"))
    password = serializers.CharField(
        label=_("Password"),
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(request=self.context.get('request'),
                                username=username, password=password)

            # The authenticate call simply returns None for is_active=False
            # users. (Assuming the default ModelBackend authentication
            # backend.)
            if not user:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = _('Must include "username" and "password".')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs

class UserLoginSerializer(serializers.ModelSerializer):
    username = serializers.SlugField(
        max_length=32,
        help_text=_(
            'Required. 32 characters or fewer. Letters, numbers, underscores or hyphens only.'
        ),
        required=True
    )
    token = serializers.CharField(allow_blank=True, read_only=True)
    name = serializers.CharField(source='profile.name', read_only=True)
    class Meta:
        model = User
        fields = [
            'username',
            'name',
            'password',
            'token',
        ]
        extra_kwargs = {"password": {"write_only": True} }
    
class UserVerifySerializer(serializers.ModelSerializer):
    username = serializers.SlugField(
        min_length=4,
        max_length=32,
        required=True
    )

    class Meta:
        model = User
        fields = (
            'username',
        )
        lookup_field = 'username'

    def update(self, instance, validated_data):
        instance.profile.role = UserRole.objects.get(id = UserRole.VERIFIED_USER)
        instance.profile.save()
        instance.save()
        return instance

class UserBanSerializer(serializers.ModelSerializer):
    username = serializers.SlugField(
        min_length=4,
        max_length=32,
        required=True
    )

    class Meta:
        model = User
        fields = (
            'username',
        )
        lookup_field = 'username'

    def update(self, instance, validated_data):
        instance.profile.role = UserRole.objects.get(id = UserRole.BANNED)
        instance.profile.save()
        instance.save()
        return instance

class UserFollowSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(
        queryset = User.objects.select_related('profile').filter(profile__role_id = UserRole.VERIFIED_USER),
        slug_field='username'
    )

    followed_user = serializers.SlugRelatedField(
        queryset = User.objects.select_related('profile').filter(profile__role_id = UserRole.VERIFIED_USER),
        slug_field='username'
    )

    class Meta:
        model = UserFollow
        fields = (
            'user',
            'followed_user'
        )
        lookup_field = 'user', 'followed_user'

    def create(self, validated_data):
        userFollow = UserFollow(
            user = validated_data.get('user'),
            followed_user = validated_data.get('followed_user')
        )
        userFollow.save()
        return userFollow

class UserUnfollowSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(
        queryset = User.objects.select_related('profile').filter(profile__role_id = UserRole.VERIFIED_USER),
        slug_field='username'
    )

    followed_user = serializers.SlugRelatedField(
        queryset = User.objects.select_related('profile').filter(profile__role_id = UserRole.VERIFIED_USER),
        slug_field='username'
    )

    class Meta:
        model = User
        fields = (
            'user',
            'followed_user'
        )
        lookup_field = 'user', 'followed_user'

    def validate(self, data):
        userFollow = UserFollow.objects.filter(user = data.get('user'), followed_user = data.get('followed_user'))
        userFollow.delete()
        return data

class UserFollowCheckSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(
        queryset = User.objects.select_related('profile').filter(profile__role_id = UserRole.VERIFIED_USER),
        slug_field='username'
    )

    followed_user = serializers.SlugRelatedField(
        queryset = User.objects.select_related('profile').filter(profile__role_id = UserRole.VERIFIED_USER),
        slug_field='username'
    )

    class Meta:
        model = User
        fields = (
            'user',
            'followed_user'
        )
        lookup_field = 'user', 'followed_user'

    def validate(self, data):
        valid = UserFollow.objects.filter(user = data.get('user'), followed_user = data.get('followed_user'))
        if not valid:
            raise serializers.ValidationError("Not Followed")
            
        return data

class UserReportSerializer(serializers.ModelSerializer):
    reporter = serializers.SlugRelatedField(
        queryset = User.objects.select_related('profile').filter(profile__role_id = UserRole.VERIFIED_USER),
        slug_field='username'
    )

    reported_user = serializers.SlugRelatedField(
        queryset = User.objects.select_related('profile').filter(profile__role_id = UserRole.VERIFIED_USER),
        slug_field='username'
    )

    class Meta:
        model = UserReport
        fields = (
            'reporter',
            'reported_user'
        )
        lookup_field = 'reporter', 'reported_user'

    def create(self, validated_data):
        userReport = UserReport(
            reporter = validated_data.get('reporter'),
            reported_user = validated_data.get('reported_user')
        )
        userReport.save()
        return userReport