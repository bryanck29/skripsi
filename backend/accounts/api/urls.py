from django.conf.urls import url
from django.urls import include, path
from django.contrib import admin

from .views import (
    UserCreateAPIView,
    UserLoginAPIView,
    UserLogoutAPIView,
    UserDetailAPIView,
    UserListAPIView,
    VerifiedUserListAPIView,
    UnverifiedUserListAPIView,
    UserDeleteAPIView,
    UserUpdateAPIView,
    UserVerifyAPIView,
    UserBanAPIView,
    UserFollowAPIView,
    UserUnfollowAPIView,
    UserSearchAPIView,
    UserFollowCheckAPIView,
    UserReportAPIView,
)

urlpatterns = [
    path('', UserListAPIView.as_view(), name='user-list'),
    path('verified/', VerifiedUserListAPIView.as_view(), name='verified-user-list'),
    path('unverified/', UnverifiedUserListAPIView.as_view(), name='unverified-user-list'),
    path('register/', UserCreateAPIView.as_view(), name='user-register'),
    path('login/', UserLoginAPIView.as_view(), name='user-login'),
    path('logout/', UserLogoutAPIView.as_view(), name='user-logout'),
    path('<slug:username>/', UserDetailAPIView.as_view(), name='user-detail'),
    path('<slug:username>/edit/', UserUpdateAPIView.as_view(), name='user-update'),
    path('<slug:username>/delete/', UserDeleteAPIView.as_view(), name='user-delete'),
    path('<slug:username>/verify', UserVerifyAPIView.as_view(), name='user-verify'),
    path('<slug:username>/ban', UserBanAPIView.as_view(), name='user-ban'),
    path('<slug:username>/follow', UserFollowAPIView.as_view(), name='user-follow'),
    path('<slug:username>/unfollow', UserUnfollowAPIView.as_view(), name='user-unfollow'),
    path('<slug:username>/check-follow', UserFollowCheckAPIView.as_view(), name='user-check-follow'),
    path('search/<slug:username>', UserSearchAPIView.as_view(), name='user-search'),
    path('<slug:username>/report', UserReportAPIView.as_view(), name='user-report'),
]
