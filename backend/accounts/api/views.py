from django.contrib.auth import get_user_model
from rest_framework import generics, views
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)
from .permissions import IsOwnerOrAdminOrReadOnly
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from accounts.models import UserRole
from accounts.models import UserFollow
from accounts.models import UserReport
from pprint import pprint

User = get_user_model()

from .serializers import (
    UserCreateSerializer,
    UserLoginSerializer,
    UserTokenSerializer,
    UserDetailSerializer,
    UserListSerializer,
    UserUpdateSerializer,
    UserVerifySerializer,
    UserBanSerializer,
    UserFollowSerializer,
    UserUnfollowSerializer,
    UserFollowCheckSerializer,
    UserReportSerializer,
)

class UserCreateAPIView(generics.CreateAPIView):
    serializer_class = UserCreateSerializer
    queryset = User.objects.all()
    permission_classes = [AllowAny]
    throttle_scope = 'create_user'

class UserDetailAPIView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserDetailSerializer
    lookup_field = 'username'
    permission_classes = [AllowAny]
    
    """ def retrieve(self, request, *args, **kwargs):
        queryset = User.objects.get(username=self.kwargs['username'])
        serializer = UserDetailSerializer(queryset, many=False, context={'request': request})
        if serializer.data:
            serializer.data.followers = User
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST) """


class UserDeleteAPIView(generics.DestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserDetailSerializer
    lookup_field = 'username'
    permission_classes = [IsOwnerOrAdminOrReadOnly]

class UserUpdateAPIView(generics.UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserUpdateSerializer
    lookup_field = 'username'
    permission_classes = [IsOwnerOrAdminOrReadOnly]
    throttle_scope = 'edit_user'

class UserListAPIView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserListSerializer
    permission_classes = [AllowAny]

class UnverifiedUserListAPIView(generics.ListAPIView):
    queryset = User.objects.select_related('profile').filter(profile__role_id = UserRole.UNVERIFIED_USER)
    serializer_class = UserListSerializer
    permission_classes = [AllowAny]

class VerifiedUserListAPIView(generics.ListAPIView):
    queryset = User.objects.select_related('profile').filter(profile__role_id = UserRole.VERIFIED_USER)
    serializer_class = UserListSerializer
    permission_classes = [AllowAny]

class UserLoginAPIView(views.APIView):
    permission_classes = [AllowAny]
    serializer_class = UserLoginSerializer
    throttle_scope = 'login'

    def post(self, request, *args, **kwargs):
        serializer = UserTokenSerializer(
            data=request.data,
            context={'request': request}
        )
        if serializer.is_valid(raise_exception=True):
            user = serializer.validated_data['user']
            token, created = Token.objects.get_or_create(user=user)
            return Response({
                'token': token.key,
                'username': user.username,
                'name': user.profile.name,
                'avatar': user.profile.avatar,
                'is_staff': user.is_staff
            }, status=HTTP_200_OK)

        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

class UserLogoutAPIView(views.APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        try:
            # simply delete the token in server side
            request.user.auth_token.delete()
            return Response(status=HTTP_200_OK)
        except:
            return Response(status=HTTP_400_BAD_REQUEST)

class UserVerifyAPIView(generics.UpdateAPIView):
    queryset = User.objects.select_related('profile').filter(profile__role_id = UserRole.UNVERIFIED_USER)
    serializer_class = UserVerifySerializer
    lookup_field = 'username'
    permission_classes = [AllowAny]
    
class UserBanAPIView(generics.UpdateAPIView):
    queryset = User.objects.select_related('profile').filter(profile__role_id = UserRole.VERIFIED_USER)
    serializer_class = UserBanSerializer
    lookup_field = 'username'
    permission_classes = [AllowAny]

class UserFollowAPIView(generics.CreateAPIView):
    queryset = UserFollow.objects.all()
    serializer_class = UserFollowSerializer
    permission_classes = [AllowAny]

class UserUnfollowAPIView(views.APIView):
    permission_classes = [AllowAny]
    serializer_class = UserUnfollowSerializer

    def post(self, request, *args, **kwargs):
        serializer = UserUnfollowSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
        
class UserSearchAPIView(generics.ListAPIView):
    serializer_class = UserListSerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        username = self.kwargs['username']
        return User.objects.filter(is_staff=False).filter(username__icontains=username)

class UserFollowCheckAPIView(views.APIView):
    permission_classes = [AllowAny]
    serializer_class = UserFollowCheckSerializer

    def post(self, request, *args, **kwargs):
        serializer = UserFollowCheckSerializer(data=request.data)
        if serializer.is_valid(raise_exception=False):
            response = {
                "isFollowed": True
            }
            return Response(response, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

class UserReportAPIView(generics.CreateAPIView):
    queryset = UserReport.objects.all()
    serializer_class = UserReportSerializer
    permission_classes = [AllowAny]
