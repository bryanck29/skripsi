from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.contrib.auth.models import User
from threads.models import Thread
from posts.models import Post
import json
import spacy
from profanity_filter import ProfanityFilter

class CreatorSerializer(serializers.ModelSerializer):
    avatar = serializers.URLField(source='profile.avatar')
    name = serializers.CharField(source='profile.name')
    class Meta:
        model = User
        fields = [
            'username',
            'name',
            'avatar',
            'is_staff'
        ]

class ThreadListSerializer(serializers.ModelSerializer):
    creator = CreatorSerializer(read_only=True)
    class Meta:
        model = Thread
        fields = (
            'id',
            'name',
            'pinned',
            'content',
            'creator',
            'created_at',
            'last_activity'
        )

class ThreadCreateSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=50, allow_blank=False)
    content = serializers.CharField(default='')
    class Meta:
        model = Thread
        fields = (
            'id',
            'name',
            'pinned',
            'content',
            'creator',
            'created_at',
            'last_activity'
        )
        read_only_fields=('id', 'pinned', 'creator', 'created_at', 'last_activity')

    def create(self, validated_data):
        name = validated_data['name']
        content = validated_data['content']

        # Get the requesting user
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        else:
            raise serializers.ValidationError('Must be authenticated to create thread')

        # Filter content and name
        nlp = spacy.load('en_core_web_sm')
        profanity_filter = ProfanityFilter(nlps={'en': nlp})
        nlp.add_pipe(profanity_filter.spacy_component, last=True)
        name = profanity_filter.censor(name)
        json_content = json.loads(content)
        json_content['blocks'][0]['text'] = profanity_filter.censor(json_content['blocks'][0]['text'])
        content = json.dumps(json_content)

        # Create the thread
        thread = Thread(
            name=name,
            content=content,
            creator=user
        )
        thread.save()
        return thread

class ThreadUpdateSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=50, allow_blank=True)
    content = serializers.CharField(allow_blank=True)
    pinned = serializers.BooleanField(default=False)
    class Meta:
        model = Thread
        fields = (
            'name',
            'pinned',
            'content',
            'creator',
            'created_at',
            'last_activity'
        )
        read_only_fields=('creator', 'created_at', 'last_activity')

    def update(self, instance, validated_data):
        # Update fields if there is any change
        for field, value in validated_data.items():
            if value != '':
                setattr(instance, field, value)
        instance.save()
        return instance

class ThreadDeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Thread
        fields = '__all__'

class ThreadPostSerializer(serializers.ModelSerializer):
    creator = CreatorSerializer(read_only=True)
    created_at = serializers.SerializerMethodField()
    class Meta:
        model = Post
        fields = [
            'id',
            'content',
            'created_at',
            'creator'
        ]
    def get_created_at(self, obj):
        return naturaltime(obj.created_at)

class ThreadDetailSerializer(serializers.ModelSerializer):
    creator = CreatorSerializer(read_only=True)
    posts = ThreadPostSerializer(many=True, read_only=True)
    created_at = serializers.SerializerMethodField()
    class Meta:
        model = Thread
        fields = (
            'id',
            'name',
            'pinned',
            'content',
            'creator',
            'created_at',
            'last_activity',
            'posts'
        )
        read_only_fields = ('id',)

    def get_created_at(self, obj):
        return naturaltime(obj.created_at)
